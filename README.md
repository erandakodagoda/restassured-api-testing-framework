# RESTAssured API Testing Framework

This is the Markdown file in **RESTAssured API Testing Framework**. If you want to learn about RESTAssured, you can read this file.

## Create files and folders

In the Project source code under the directory of payloads (/payloads) will be used to store the JSON payload files and under employee (src/test/java/globe/api/admin/employee) the API controllers classes will be placed.

### Usage

Sample **.json** file
```json
{
  "employee_name": "Test Employee",
  "employee_salary": "20000",
  "employee_age": "23",
  "profile_image": ""
}
```

Sample **Controller** File
```java
public class EmployeeController extends BaseClass {
    String baseURL;

    @Test(priority = 1)
    public void createEmployee() throws IOException {
        baseURL = getURL();
        String createEmployeeEndpoint = "/create";

        //Setting up Base URL
        baseURI = baseURL;

        //Verifying the create request and success response
        given()
                .header("accept", "*/*")
                .contentType(ContentType.JSON)
                .body(getGeneratedString("add-employee.json"))

                .when()
                .post(createEmployeeEndpoint)

                .then()
                .assertThat().statusCode(200);
    }

    @Test(priority = 2)
    public void getAllEmployees() throws IOException {
        baseURL = getURL();
        String getBulkEndpoint = "/employees";

        //Setting up Base URL
        baseURI = baseURL;

        //Verifying the Status code & response
        given()
                .header("accept", "*/*")

                .when()
                .get(getBulkEndpoint)

                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .contentType(ContentType.JSON)
                .and()
                .body("[1].id", equalTo("1"));
    }

}
```

### Installation

RESTAssured API Testing Framework needs below tools
[Java Development Kit](https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html) v1.8 or v1.7.
[Apache Maven](https://maven.apache.org/download.cgi) v3.6.3 or above.

Execute the test's.

```sh
$ cd restassured-api-testing-framework
$ mvn test -DsuiteXmlFile=E:\Workspace\DevRepo\restassured-api-testing-framework\testng.xml
```

### Plugins

RESTAssured API Testing Framework is currently extended with the following plugins. Instructions on how to use them in your own application are linked below.

| Plugin | Documentation |
| ------ | ------ |
| RESTAssured | [RESTAssured/documentation][PlDb] |
| TestNG | [TestNG/documentation][PlDh]|

## License
----
All Rights Received by Eranda Kodagoda.

   [PlDb]: <https://github.com/rest-assured/rest-assured/wiki/GettingStarted>
   [PlDh]: <https://testng.org/doc/documentation-main.html>