package globe.api.admin.employee;


import globe.BaseClass;
import io.restassured.http.ContentType;
import org.testng.annotations.Test;

import java.io.IOException;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

/**
 * @author : Eranda Kodagoda
 *  * @date : August 10, 2020
 *  * @version : 1.0
 *  * @copyright : © 2020 Eranda Kodagoda
 *  */

public class EmployeeController extends BaseClass {
    String baseURL;

    @Test(priority = 1)
    public void createEmployee() throws IOException {
        baseURL = getURL();
        String createEmployeeEndpoint = "/create";

        //Setting up Base URL
        baseURI = baseURL;

        //Verifying the create request and success response
        given()
                .header("accept", "*/*")
                .contentType(ContentType.JSON)
                .body(getGeneratedString("add-employee.json"))

                .when()
                .post(createEmployeeEndpoint)

                .then()
                .assertThat().statusCode(200);
    }

    @Test(priority = 2)
    public void getAllEmployees() throws IOException {
        baseURL = getURL();
        String getBulkEndpoint = "/employees";

        //Setting up Base URL
        baseURI = baseURL;

        //Verifying the Status code & response
        given()
                .header("accept", "*/*")

                .when()
                .get(getBulkEndpoint)

                .then()
                .assertThat()
                .statusCode(200)
                .and()
                .contentType(ContentType.JSON)
                .and()
                .body("[1].id", equalTo("1"));
    }

}
